# vim: set et ts=2 sts=2 sw=2 :
# LiveScript 1.4.0

require! {
  path
  gulp
  colors
  through2
  \gulp-watch : watch
  \gulp-batch : batch
  \child_process : {spawn}
  fs: {readdir-sync, stat-sync, access, F_OK}
}

const names =
  readdir-sync \local .filter (-> stat-sync "local/#it" .is-directory!)

const watch-tasks = names.map (name)->
  
  const task-name = "#{name}-watch"
  const src-dir   = "local/#name"
  const dest-dir  = "remote/#name"
  
  gulp.task task-name, !-> watch "#src-dir/**/*", batch (events, done)!->
    events
      .pipe through2.obj (f, e, cb)!->
        (do cb ; return) unless f.event in <[ add change ]>
        
        const file      = f.path.slice f.base.length
        const dest-file = path.resolve dest-dir, file
        
        console.info "#{new Date} --->\n  UPDATED: #file".blue
        
        const stdio      = [null, null, process.stderr]
        const file-to-cp = path.resolve src-dir, file .slice process.cwd!.length+1
        
        const p =
          [ file-to-cp, dest-file ]
          |> (.map path.dirname)
          |> (.map (dir)->
                resolve, reject <-! new Promise _
                err <-! access dir, F_OK
                if err?
                  console.info "Creating directory: #dir".cyan
                  spawn \mkdir [\-p, dir], {stdio} .on \close (status)!->
                    if status isnt 0
                      console.error "
                        ===> BLEAT SUKA! 'mkdir' exit status: #status
                        \ for dir: #dir <===".red
                      reject new Error "'mkdir' fell with status: #status"
                    else do resolve
                else do resolve)
          |> Promise.all
        
        <-! (!-> p .then it .catch cb)
        
        spawn \cp, [file-to-cp, dest-file], {stdio} .on \close (status)!->
          if status is 0
            console.info "#{new Date} ~~~>\n  DONE: #file".green
            do cb
            return
            
          console.error "===> BLEAT SUKA! 'cp' exit status: #status <===".red
          cb new Error "'cp' fell with status: #status"
          
      .on \finish, done
      
  task-name

gulp.task \watch, watch-tasks
gulp.task \default, <[watch]>
